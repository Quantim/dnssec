#!/usr/bin/python3
import logging
import pprint
import os
import time

import dns.query

import labcfg

def gather(zonename, ipaddr):
    logging.debug('querying %s for %s SOA', ipaddr, zonename)
    soa_query = dns.message.make_query(zonename, dns.rdatatype.SOA)
    try:
        soa_answer = dns.query.tcp(soa_query, ipaddr, timeout=0.1)
    except Exception as ex:
        logging.warning('query failed %s: %s', ipaddr, ex)
        return str(ex)
    if soa_answer.rcode() != dns.rcode.NOERROR:
        return dns.rcode.to_text(soa_answer.rcode())
    try:
        soa_rrset = soa_answer.find_rrset(dns.message.ANSWER, zonename, dns.rdataclass.IN, dns.rdatatype.SOA)
    except KeyError:
        return 'NOERROR but no SOA?!'
    try:
        return 'NOERROR serial {}'.format(soa_rrset[0].serial)
    except IndexError:
        return 'NOERROR but empty SOA RRset?!'

def gather_all(labcfg):
    results = {}
    for zonename, ipaddr in labcfg.zone2ip.items():
        # by default query auth server
        # for validation point override IP server's IP with an validating resolver
        ipaddr = '127.0.0.1'
        results[zonename] = gather(zonename, ipaddr)
    return results

def display(results):
    os.system('clear')
    for zonename in sorted(results.keys()):
        print('{}\t{}'.format(zonename, results[zonename]))

if __name__ == '__main__':
    logging.basicConfig(level=logging.ERROR)

    while True:
        results = gather_all(labcfg)
        display(results)
        time.sleep(1)
