#!/usr/bin/python3
import ipaddress
import pprint

import dns.name

subnet_prefix = '192.168.20' #
serveraddress = ['100', '102', '103', '104', '105', '106', '107', '108', '110', '111', '112', '116', '151', '160']
ds_ttl = 10

zone2ip = {dns.name.from_text('z{}.skoleni.'.format(stud)): '{}.{}'.format(subnet_prefix, stud)
            for stud in serveraddress}

zone2ip = {dns.name.from_text('knot-resolver.cz'): '194.0.13.1'}

if __name__ == '__main__':
    print('ds_ttl = {}'.format(ds_ttl))
    print('subnet_prefix = {}'.format(subnet_prefix))
    pprint.pprint(zone2ip)
