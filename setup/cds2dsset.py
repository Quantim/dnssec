#!/usr/bin/python3
import ipaddress
import logging

import dns.query

import labcfg

for zonename, ipaddr in labcfg.zone2ip.items():
    logging.debug('querying %s for %s CDS', ipaddr, zonename)
    # query for CDS
    cds_query = dns.message.make_query(zonename, 59)
    try:
        cds_answer = dns.query.tcp(cds_query, ipaddr, timeout=0.1)
    except Exception as ex:
        logging.warning('query failed %s: %s', ipaddr, ex)
        continue
    logging.debug(cds_answer.to_text())
    if cds_answer.rcode() != dns.rcode.NOERROR:
        logging.warning('invalid RCODE from %s', ipaddr)
        continue
    try:
        cds_rrset = cds_answer.find_rrset(dns.message.ANSWER, zonename, dns.rdataclass.IN, 59)
    except KeyError:
        logging.debug('no CDS for %s', zonename)
        continue

    # convert to DS rrset
    ds_rrset = cds_rrset._clone()
    ds_rrset.rdtype = 43  # ugly hack, change to DS
    ds_rrset.ttl = labcfg.ds_ttl

    with open('dsset-{}'.format(zonename.relativize(dns.name.root)), 'w') as dsset:
        dsset.write(ds_rrset.to_text())
        dsset.write('\n')
