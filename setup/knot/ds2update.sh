#!/bin/bash

FILES=`ls dsset*`

echo "server 127.0.0.1" > nsupdate-in
echo "zone skoleni." >> nsupdate-in

for FILE in $FILES; do
   LINE=`head -n1 $FILE`
   ZONE=`echo $LINE | awk '{print $1}'`
   echo "update delete $ZONE DS" >> nsupdate-in
   echo "update add $ZONE 20 IN DS `echo $LINE | awk '{print $4, $5, $6, $7}'`" >> nsupdate-in
done;

echo "show" >> nsupdate-in
echo "send" >> nsupdate-in
nsupdate nsupdate-in
