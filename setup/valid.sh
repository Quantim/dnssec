DOMAINS=("z105.skoleni" "z107.skoleni" "z160.skoleni" "z151.skoleni" "z103.skoleni" "z102.skoleni" "z108.skoleni")
RESOLVER="10.10.0.111"
while [ 1 -eq 1 ]; do
	cat > domain.output <<<""
	for DOM in $DOMAINS; do
		OUT=$(dig SOA $DOM +dnssec +multi +time=1 @$RESOLVER)
		STATUS=$(grep status <<<$OUT | awk -F'[, ]' '{ print $7 }')
		FLAGS=$(grep flags: <<<$OUT | grep ad)
		if [ -z $FLAGS ]; then
			VALID="UNSECURE"
		else
			VALID="SECURE"
		fi
		
	       	echo "                         "  $DOM ":" $STATUS ":" $VALID >> domain.output
	done
	sleep 5
	clear
	echo "\n\n\n\n\n\n\n\n\n\n"
	cat domain.output
done
